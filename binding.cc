#include <napi.h>

#include "./src/usr_include/STBApi/STBWrap.h"


Napi::String SayHi(const Napi::CallbackInfo& info) {
  Napi::Env env = info.Env();

    HVC_RESULT *pHVCResult = NULL;
  pHVCResult = (HVC_RESULT *)malloc(sizeof(HVC_RESULT));

  int nSTBFaceCount;
  STB_FACE *pSTBFaceResult;
  int nSTBBodyCount;
  STB_BODY *pSTBBodyResult;

  int ret = STB_Init(STB_FUNC_BD | STB_FUNC_DT | STB_FUNC_PT | STB_FUNC_AG | STB_FUNC_GN);

  return Napi::String::New(env, "Hi!");
}

Napi::Object init(Napi::Env env, Napi::Object exports) {
    exports.Set(Napi::String::New(env, "sayHi"), Napi::Function::New(env, SayHi));
    return exports;
};

NODE_API_MODULE(stb, init);

{
  "targets": [
  {
    'target_name': 'bindings',
    "include_dirs" : [
      "<!@(node -p \"require('node-addon-api').include\")",
      "<(module_root_dir)/src/usr_include"
      "<(module_root_dir)/src/src"
    ],
      "target_name": "stb",
      "sources": [ 
        "src/binding.c",
        "src/usr_include/HVCApi.c",
        "src/usr_include/STBWrap.c",

        "src/src/Interface.c",
        "src/src/STBAPI.c",
        "src/src/STBFaceInfo.c",
        "src/src/STBMakeResult.c",
        "src/src/STBTracking.c",
        "src/src/STBValidValue.c",

        "src/src/STBFrAPI.c",
        "src/src/STBFrValidValue.c",
        "src/src/SdkSTBFr.c",
        "src/src/FrInterface.c",

        "src/src/PeInterface.c",
        "src/src/SdkSTBPe.c",
        "src/src/STBPeAPI.c",
        "src/src/STBPeValidValue.c",

        "src/src/TrInterface.c",
        "src/src/SdkSTBTr.c",
        "src/src/STBTrAPI.c",
        "src/src/STBTrValidValue.c",

      ],
      'defines': [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ],
      'conditions': [
        ['OS=="mac"', {
          'xcode_settings': {
            # 'GCC_ENABLE_CPP_RTTI': 'YES',
            "CLANG_CXX_LANGUAGE_STANDARD": "c++11"
          }
        },]
      ],
      'libraries+':[
        '<(module_root_dir)/src/lib/STB.a',
        '<(module_root_dir)/src/lib/STB.lib',
        # '-WL,-soname=<(module_root_dir)/src/lib/libSTB.so',
      ],
    }
  ]
}

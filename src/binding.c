#include <node_api.h>
#include <stdio.h>

#include "usr_include/STBWrap.h"

#define STB_RETRYCOUNT_DEFAULT               2            /* Retry Count for STB */
#define STB_POSSTEADINESS_DEFAULT           30            /* Position Steadiness for STB */
#define STB_SIZESTEADINESS_DEFAULT          30            /* Size Steadiness for STB */
#define STB_PE_FRAME_DEFAULT                10            /* Complete Frame Count for property estimation in STB */
#define STB_PE_ANGLEUDMIN_DEFAULT          -15            /* Up/Down face angle minimum value for property estimation in STB */
#define STB_PE_ANGLEUDMAX_DEFAULT           20            /* Up/Down face angle maximum value for property estimation in STB */
#define STB_PE_ANGLELRMIN_DEFAULT          -20            /* Left/Right face angle minimum value for property estimation in STB */
#define STB_PE_ANGLELRMAX_DEFAULT           20            /* Left/Right face angle maximum value for property estimation in STB */
#define STB_PE_THRESHOLD_DEFAULT           300            /* Threshold for property estimation in STB */



napi_value print (napi_env env, napi_callback_info info) {
  napi_value argv[1];
  size_t argc = 1;

  napi_get_cb_info(env, info, &argc, argv, NULL, NULL);

  if (argc < 1) {
    napi_throw_error(env, "EINVAL", "Too few arguments");
    return NULL;
  }

  char str[1024];
  size_t str_len;

  if (napi_get_value_string_utf8(env, argv[0], (char *) &str, 1024, &str_len) != napi_ok) {
    napi_throw_error(env, "EINVAL", "Expected string");
    return NULL;
  }

  printf("Printed from C: %s\n", str);

  HVC_RESULT *pHVCResult = NULL;
  pHVCResult = (HVC_RESULT *)malloc(sizeof(HVC_RESULT));

  int nSTBFaceCount;
  STB_FACE *pSTBFaceResult;
  int nSTBBodyCount;
  STB_BODY *pSTBBodyResult;
// 
  int ret = STB_Init(STB_FUNC_BD | STB_FUNC_DT | STB_FUNC_PT | STB_FUNC_AG | STB_FUNC_GN);
  ret = STB_SetTrParam(STB_RETRYCOUNT_DEFAULT, STB_POSSTEADINESS_DEFAULT, STB_SIZESTEADINESS_DEFAULT);
  // if ( ret != 0 )
  printf("\nHVCApi(STB_SetTrParam) Error : %d\n", ret);
  
  ret = STB_SetPeParam(STB_PE_THRESHOLD_DEFAULT, STB_PE_ANGLEUDMIN_DEFAULT, STB_PE_ANGLEUDMAX_DEFAULT, STB_PE_ANGLELRMIN_DEFAULT, STB_PE_ANGLELRMAX_DEFAULT, STB_PE_FRAME_DEFAULT);
  // if ( ret != 0 ) {
      printf("\nHVCApi(STB_SetPeParam) Error : %d\n", ret);
  // }
  
  printf("Body detect ID: %d\n", 888);

  if ( STB_Exec(pHVCResult->executedFunc, pHVCResult, &nSTBFaceCount, &pSTBFaceResult, &nSTBBodyCount, &pSTBBodyResult) == 0 ) {
    for ( int i = 0; i < nSTBBodyCount; i++ )
    {
        if ( pHVCResult->bdResult.num <= i ) break;
        printf("Body detect ID: %d\n", pSTBBodyResult[i].nDetectID);
    }
  //   for ( int i = 0; i < nSTBFaceCount; i++ )
  //   {
  //       if ( pHVCResult->fdResult.num <= i ) break;
  //       printf("Face detect ID: %d\n", pSTBFaceResult[i].nDetectID);
  //   }
  }


  return NULL;
}


napi_value init_all (napi_env env, napi_value exports) {

  // Print
  napi_value print_fn;
  napi_create_function(env, NULL, 0, print, NULL, &print_fn);
  napi_set_named_property(env, exports, "print", print_fn);

  return exports;
}

NAPI_MODULE(stb, init_all)
